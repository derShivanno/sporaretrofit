package com.shivanno.testhackranck.sporaretrofit;

/**
 * Created by shivanno on 16/03/17.
 */

public class ModeloResponse {

    private String transaction;
    private String balance;
    private String name;
    private String email;
    private String prueba;

    public ModeloResponse(){

    }


    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
