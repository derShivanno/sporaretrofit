package com.shivanno.testhackranck.sporaretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        callRetrofit();
    }

    void callRetrofit(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.cinepolis.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ModelRequest bodyRequest = new ModelRequest();
        bodyRequest.setCustomerMobile("9634454475");

        SporaService sporaService = retrofit.create(SporaService.class);
        sporaService.getTransactions("true", bodyRequest).enqueue(new Callback<ModeloResponse>() {
            @Override
            public void onResponse(Call<ModeloResponse> call, Response<ModeloResponse> response) {
                Log.e("tag", "onsuccess");
            }

            @Override
            public void onFailure(Call<ModeloResponse> call, Throwable t) {
                Log.e("tag", "onError");
            }
        });

    }



    private OkHttpClient okHttpClient;
    private static final int TIEMOUT_SECONDS = 60;
    protected void configRetrofitClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        okHttpClient = new OkHttpClient.Builder()
                .readTimeout(TIEMOUT_SECONDS, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(TIEMOUT_SECONDS, TimeUnit.SECONDS)
                .build();
    }
}
