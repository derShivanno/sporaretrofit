package com.shivanno.testhackranck.sporaretrofit;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shivanno on 17/03/17.
 */

public class ModelRequest {
    @SerializedName("customer_mobile")
    private String customerMobile;

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }
}
