package com.shivanno.testhackranck.sporaretrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.HEAD;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by shivanno on 16/03/17.
 */

public interface SporaService {
    //@Headers("Content-Type:application/json")
    @POST("v1/moviequest/customer_history")
    Call<ModeloResponse> getTransactions(
            //@Headers("Content-Type:application/json"),
            @Query( "include_transactions") String includeTransactions,
            @Body ModelRequest body );
}
